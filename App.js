import 'react-native-gesture-handler';
import React from 'react'
import { NavigationNativeContainer } from '@react-navigation/native'
import AuthLoading from './navigation/AuthLoading'

// Redux
import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux'
import authReducers from './store/authReducers'

const store = createStore(
  combineReducers({
    authReducers
  }), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

const App = () => {
  return (
    <Provider store={store}>
      <NavigationNativeContainer>
        <AuthLoading />
      </NavigationNativeContainer>
    </Provider>
  )
}

export default App
