import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {faHome,faFont,faBars,faStopwatch,faDoorOpen } from '@fortawesome/free-solid-svg-icons'

export const HomeIcon = (props) => (
    <FontAwesomeIcon {...props} icon={faHome}/>
)

export const FaqIcon = (props) => (
    <Icon {...props} name='exclamation-circle'/>
)

export const AlphabetIcon = (props) => (
    <FontAwesomeIcon icon={faFont}/>
)

export const MenuHamburger = (props) => (
    <FontAwesomeIcon {...props} icon={faBars}/>
)

export const Counter = (props) => (
    <FontAwesomeIcon {...props} icon={faStopwatch}/>
)

export const Logout = (props) => (
    <FontAwesomeIcon {...props} icon={faDoorOpen}/>
)


