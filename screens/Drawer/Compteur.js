import React,{useState} from 'react';
import { StyleSheet, Text, View,Button } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
});

const Compteur  = ({navigation}) => {
  const [count, setCount] = useState(0);
  if (count === 10) {
    setCount(0)
  }
  navigation.setOptions({
    headerLeft: () => (
        <Button
            onPress={() => setCount(count + 1)}
            title="Update count"
        />
    ),
  });
  return (
    <View style={styles.container}>
      <Text style={{textAlign:'center'}}>Increment d'un compteur exemple avec setOptions de react navivation v.5</Text>
      <Text style={{fontSize:44}}>{count}</Text>
      <Button disabled={count === 0} title="RAZ" onPress={() => setCount(0)}/>
    </View>
  );
}


export default Compteur