import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
});

const Status  = () => {
  return (
    <View style={styles.container}>
      <Text>Status</Text>
    </View>
  );
}

export default Status