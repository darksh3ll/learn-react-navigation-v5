import React from 'react';
import {KeyboardAvoidingView, SafeAreaView, StyleSheet, Text, View} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
});

const Progress  = () => {
  return (
    <SafeAreaView style={styles.container}>
      <Text>PROGRESS</Text>
    </SafeAreaView>
  );
}

export default Progress