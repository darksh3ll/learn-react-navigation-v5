import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
});


const Todos  = () => {
  return (
    <View style={styles.container}>
      <Text>TODOS</Text>
    </View>
  );
}

export default Todos