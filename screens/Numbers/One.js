import React,{useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity, AsyncStorage,Button} from 'react-native';
import ConfirmButton from "../../components/ConfirmButton";
import { useDispatch, useSelector } from 'react-redux'
const styles = StyleSheet.create({
  container: {

  },
});

const One  = ({navigation}) => {
  const dispatch = useDispatch()
  const logout = async () => {
    await AsyncStorage.clear()
    await dispatch({ type: 'LOGOUT'});
  }
  navigation.setOptions({
    headerRight: () => (
        <Button
            onPress={logout}
            title="Déconnexion"
        />
    ),
  });
  return (
      <TouchableOpacity
          onPress={() => navigation.navigate('Two')}
          style={{flex:1,justifyContent:'center',alignItems:'center'}}
      >
        <Text>One</Text>
      </TouchableOpacity>
  );
}

export default One