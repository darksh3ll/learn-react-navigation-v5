import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const styles = StyleSheet.create({
  container: {

  },
});

const Three  = ({navigation}) => {
  return (
      <TouchableOpacity
          onPress={() => navigation.popToTop()}
          style={{flex:1,justifyContent:'center',alignItems:'center'}}
      >
        <Text style={{fontSize:24}}>Three</Text>
        <Text style={{paddingTop:10}}>navigation.popToTop()</Text>
        <Text style={{textAlign:'center',paddingTop:10}}> revenir à l'itinéraire supérieur de la pile, en ignorant tous les autres écrans.</Text>
      </TouchableOpacity>
  );
}

export default Three