import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const styles = StyleSheet.create({
  container: {

  },
});

const Two  = ({navigation}) => {
  return (
      <TouchableOpacity
          onPress={() => navigation.navigate('Three')}
          style={{flex:1,justifyContent:'center',alignItems:'center'}}
      >
        <Text>Two</Text>
      </TouchableOpacity>
  );
}

export default Two