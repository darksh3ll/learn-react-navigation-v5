import React from 'react';
import {Text,TouchableOpacity,View,Button } from 'react-native';

export default function A({navigation}) {
  return (
        <TouchableOpacity
        onPress={() => navigation.navigate("B")}
        style={{flex:1,justifyContent:'center',alignItems:'center'}}
    >
      <Text>A</Text>
    </TouchableOpacity>
  );
}
