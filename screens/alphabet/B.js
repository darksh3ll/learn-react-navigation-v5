import React from 'react';
import { View,Text,TouchableOpacity } from 'react-native';


export default function B({navigation}) {
  return (
    <TouchableOpacity
        style={{flex:1,justifyContent:'center',alignItems:'center'}}
        onPress={() => navigation.navigate('C')}>
      <Text>B</Text>
    </TouchableOpacity>
  );
}
