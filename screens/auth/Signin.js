import React, { useState, useRef } from 'react'
import { View, Text, StyleSheet, AsyncStorage, ScrollView, KeyboardAvoidingView } from 'react-native'
import InputCell from '../../components/InputCell'
import ConfirmButton from '../../components/ConfirmButton'
import { useDispatch, useSelector } from 'react-redux'
import axios from 'axios'
import Loading from '../../components/Loading'
import errorMessage from '../../utils/errorMessage'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  }
})
const SignIn = ({ navigation }) => {
  const dispatch = useDispatch()
  // Ref
  const passwordRef = useRef()

  // state
  const [identifiant, setIdentifiant] = useState('')
  const [password, setPassword] = useState('')
  const [loading, setLoading] = useState(false)

  // Ajout Token storage device
  const _signInAsync = async (token) => {
    await AsyncStorage.setItem('userToken', token)
    dispatch({ type: 'LOGIN', payload: token })
  }

  // Sign User API@gmail.fr
  const getSigin = async () => {
    if (identifiant.length > 0 && password.length > 0) {
      setLoading(true)
      await axios.post('https://bagmobile.makeitdev.fr/api/fr/users/login', {
        username: identifiant,
        password: password
      })
        .then((res) => {
          setLoading(false)
          _signInAsync(res.data.data.token)
        })
        .catch((error) => {
          setLoading(false)
          if (error) {
            errorMessage('login ou mot de passe incorrecte')
          }
        })
    }
  }

  return (
    <KeyboardAvoidingView style={styles.container} behavior='padding' enabled>
      <Loading loading={loading} />
      <RenderContainer
        passwordRef={passwordRef}
        navigation={navigation}
        onPress={getSigin}
        identifiant={identifiant}
        setIdentifiant={setIdentifiant}
        password={password}
        setPassword={setPassword}
      />
    </KeyboardAvoidingView>
  )
}

const RenderContainer = ({ identifiant, setIdentifiant, password, setPassword, onPress, navigation, passwordRef }) => (
  <View>
    <InputCell
      value={identifiant}
      onChangeText={(text) => setIdentifiant(text)}
      placeholder='Identifiant'
      returnKeyType='next'
      onSubmitEditing={() => passwordRef.current.focus()}
    />
    <InputCell
      returnKeyType='done'
      secureTextEntry
      value={password}
      onChangeText={(text) => setPassword(text)}
      placeholder='Mot de passe'
      focus={passwordRef}
    />
    <ConfirmButton onPress={onPress} title='Se connecter' />
    <Text onPress={() => navigation.navigate('ResetPassword')} style={{ textAlign: 'center' }}>Mot de passe oublié ?</Text>
  </View>
)

SignIn.navigationOptions = {
  title: '',
  header: null

}

export default SignIn
