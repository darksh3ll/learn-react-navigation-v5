import React from 'react'
import { View, Text, TouchableOpacity, Button, Image } from 'react-native'

// import { Container } from './styles';

export default function Welcome ({ navigation }) {
  return (
    <TouchableOpacity onPress={() => navigation.navigate('Signin')} style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#60499d' }}>
      <Text style={{ color: 'white', fontSize: 24 }}>Learn React Navigation V.5 😃</Text>
    </TouchableOpacity>
  )
}
