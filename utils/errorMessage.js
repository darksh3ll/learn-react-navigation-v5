import { Alert } from 'react-native'

export default (message, onPress) => {
  Alert.alert(
      `${message}`,
      '',
      [
        { text: 'OK', onPress: onPress }
      ]
  )
}
