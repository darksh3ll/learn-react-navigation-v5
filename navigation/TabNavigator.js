import React from 'react'
import { View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Badge } from 'react-native-elements'
import AlphabetStackNavigator from './AlphabetStackNavigator'
import NumberStackNavigator from './NumberStackNavigator'
import TodoTabs from '../navigation/TodoTabs'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faHome, faFont, faSortNumericDown } from '@fortawesome/free-solid-svg-icons'
const Tab = createBottomTabNavigator()

const IconWithBadge = ({ value, icon }) => {
  return (
    <View>
      <FontAwesomeIcon
        icon={icon}
        size={25}
      />
      {value > 0 &&
        <Badge
          value={value > 99 ? '99+' : value}
          status='error'
          containerStyle={{ position: 'absolute', top: -3, right: -6 }}
        />}
    </View>
  )
}

const TabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size, displayBadge }) => {
          if (route.name === 'Alphabet') {
            return <IconWithBadge icon={faFont} value={100} />
          } else if (route.name === 'Numbers') {
            return <IconWithBadge icon={focused ? faSortNumericDown : faSortNumericDown} />
          }
        }
      })}
      tabBarOptions={{
        activeTintColor: 'white',
        inactiveTintColor: 'gray',
        activeBackgroundColor: '#694fad'
      }}
    >
      <Tab.Screen
        name='Alphabet'
        component={AlphabetStackNavigator}
        options={{
          tabBarLabel: 'ALPHABET'
        }}
      />
      <Tab.Screen name='Numbers' component={NumberStackNavigator} />
      <Tab.Screen name='Todos' component={TodoTabs} />
    </Tab.Navigator>
  )
}

export default TabNavigator
