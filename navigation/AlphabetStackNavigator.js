import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { StyleSheet, Text, TouchableOpacity, AsyncStorage, Button } from 'react-native'
// import screens
import A from '../screens/alphabet/A'
import B from '../screens/alphabet/B'
import C from '../screens/alphabet/C'
import { MenuHamburger } from '../assets/icons'

const Stack = createStackNavigator()

const AlphabetStackNavigator = ({ navigation }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackTitle: true, // Désactive le titre du button Back
        // headerBackTitle:'retour', //Change le titre duu button Back
        // headerTitle:null, //desactive le titre de l'en-tête
        headerTintColor: 'white',
        headerStyle: { backgroundColor: '#60499d' },
        headerRight: () => (
          <TouchableOpacity style={{ padding: 10 }} onPress={() => navigation.openDrawer()}>
            <MenuHamburger size={30} color='white' />
          </TouchableOpacity>
          // <Button
          //     onPress={() => navigation.openDrawer()}
          //     title="drawer"
          //     color="#fff"
          // />
        )
      }}
    >
      <Stack.Screen
        name='A' component={A}
      />
      <Stack.Screen name='B' component={B} />
      <Stack.Screen name='C' component={C} />
    </Stack.Navigator>
  )
}

export default AlphabetStackNavigator
