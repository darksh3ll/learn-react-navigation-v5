import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Todos from "../screens/todos/Todos";
import Progress from "../screens/todos/Progress";
import {SafeAreaView} from "react-native";
import Status from "../screens/todos/Status";

const Tab = createMaterialTopTabNavigator();

function MyTabs() {
  return (
      <SafeAreaView style={{flex:1}}>
        <Tab.Navigator>
          <Tab.Screen name="Progress" component={Progress} />
          <Tab.Screen name="Todos" component={Todos} />
          <Tab.Screen name="Status" component={Status} />
        </Tab.Navigator>
      </SafeAreaView>

  );
}

export default  MyTabs