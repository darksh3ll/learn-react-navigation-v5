import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

// import screens
import One from '../screens/Numbers/One'
import Two from '../screens/Numbers/Two'
import Three from '../screens/Numbers/Three'
import { TouchableOpacity } from 'react-native'
import { MenuHamburger } from '../assets/icons'

const Stack = createStackNavigator()

const NumberStackNavigator = ({ navigation }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackTitle: true,
        headerTitle: null,
        headerTintColor: 'white',
        headerStyle: { backgroundColor: '#60499d' },
        headerRight: () => (
          <TouchableOpacity style={{ padding: 10 }} onPress={() => navigation.openDrawer()}>
            <MenuHamburger size={30} color='white' />
          </TouchableOpacity>
          // <Button
          //     onPress={() => navigation.openDrawer()}
          //     title="drawer"
          //     color="#fff"
          // />
        )
      }}

    >
      <Stack.Screen name='One' component={One} />
      <Stack.Screen name='Two' component={Two} />
      <Stack.Screen name='Three' component={Three} />
    </Stack.Navigator>
  )
}

export default NumberStackNavigator
