import React from 'react'
import { AsyncStorage, StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native'
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer'
import bg from '../assets/bg.jpg'
import Faq from '../screens/Drawer/Faq'
import Compteur from '../screens/Drawer/Compteur'
import TabNavigator from './TabNavigator'
import { Counter, FaqIcon, HomeIcon, Logout, MenuHamburger } from '../assets/icons'
import { createStackNavigator } from '@react-navigation/stack'
import One from '../screens/Numbers/One'
import { useDispatch } from 'react-redux'
const Drawer = createDrawerNavigator()
const Stack = createStackNavigator()

const TestScreen = ({ navigation }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackTitle: true, // Désactive le titre du button Back
        // headerBackTitle:'retour', //Change le titre duu button Back
        // headerTitle:null, //desactive le titre de l'en-tête
        headerTintColor: 'white',
        headerStyle: { backgroundColor: '#60499d' },
        headerRight: () => (
          <TouchableOpacity style={{ padding: 10 }} onPress={() => navigation.openDrawer()}>
            <MenuHamburger size={30} color='white' />
          </TouchableOpacity>
        )
      }}
    >
      <Stack.Screen name='counter' component={Compteur} />
    </Stack.Navigator>
  )
}

function CustomDrawerContent ({ props, dispatch }) {
  const logout = async () => {
    await AsyncStorage.clear()
    await dispatch({ type: 'LOGOUT' })
  }
  return (
    <DrawerContentScrollView {...props}>
      <View style={{ flex: 1 }}>
        <Image
          resizeMode='cover'
          style={{ width: '100%', height: 150 }}
          source={bg}
        />
      </View>

      <DrawerItemList {...props} />
      <DrawerItem
        label='Déconnexion'
        icon={Logout}
        onPress={logout}
      />
    </DrawerContentScrollView>
  )
}

const AppDrawerNavigator = () => {
  const dispatch = useDispatch()
  return (
    <Drawer.Navigator
      drawerContent={props => CustomDrawerContent({ props, dispatch })}
      hideStatusBar
      drawerStyle={{
        backgroundColor: '#694fad',
        width: 240
      }}
      drawerContentOptions={{
        activeTintColor: 'black',
        inactiveTintColor: 'white',
        itemStyle: { marginVertical: 0 }
      }}
    >
      <Drawer.Screen
        options={{ title: 'Home', drawerIcon: HomeIcon }}
        name='Home'
        component={TabNavigator}
      />
      <Drawer.Screen
        options={{ title: 'Faq', drawerIcon: FaqIcon }}
        name='Faq'
        component={Faq}
      />
      <Drawer.Screen
        options={{ title: 'Compteur', drawerIcon: Counter }}
        name='compteur'
        component={TestScreen}
      />
    </Drawer.Navigator>
  )
}

export default AppDrawerNavigator
