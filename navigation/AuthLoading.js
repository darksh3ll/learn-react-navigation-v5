import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { AsyncStorage } from 'react-native'

// import screens
import AuthStackNavigator from './AuthStackNavigator'
import TabNavigator from './TabNavigator'
import AppDrawerNavigator from './AppDrawerNavigator'

const AuthLoading = () => {
  // Redux
  const userToken = useSelector(state => state.authReducers.userToken)
  const dispatch = useDispatch()

  useEffect(() => {
    const bootstrapAsync = async () => {
      let userToken
      try {
        userToken = await AsyncStorage.getItem('userToken')
        dispatch({ type: 'LOGIN', payload: userToken })
      } catch (e) {
      }
    }
    bootstrapAsync()
  }, [])

  return (
    userToken ? <AppDrawerNavigator /> : <AuthStackNavigator />
  )
}

export default AuthLoading
