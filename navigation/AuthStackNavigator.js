import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

// import screens
import Signin from '../screens/auth/Signin'
import SignUp from '../screens/auth/SignUp'
import Welcome from '../screens/auth/Welcome'
import ResetPassword from '../screens/auth/ResetPassword'

const Stack = createStackNavigator()
const config = {
  animation: 'spring',
  config: {
    stiffness: 1000,
    damping: 500,
    mass: 3,
    overshootClamping: true,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01
  }
}
const AuthStackNavigator = () => (
  <Stack.Navigator

    headerMode='screen'
    initialRouteName='Welcome'
    screenOptions={{
      headerMode: 'float',
      headerTitle: null,
      headerTintColor: 'white',
      headerStyle: { backgroundColor: '#60499d' },

    }}
  >
    <Stack.Screen options={{ headerShown: false }} name='Welcome' component={Welcome} />
    <Stack.Screen name='Signin' component={Signin} />
    <Stack.Screen

        name='SignUp' component={SignUp} />
    <Stack.Screen
        options={{
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
        name='ResetPassword' component={ResetPassword} />
  </Stack.Navigator>
)

export default AuthStackNavigator
