import React from 'react'
import { StyleSheet } from 'react-native'
import { Button } from 'react-native-elements'

const styles = StyleSheet.create({
  m20: {
    margin: 20
  },
  colorWhite: {
    color: 'white'
  },
  backgroundColorblue: {
    backgroundColor: '#60499d'
  }
})

const ConfirmButton = ({ title, onPress, disabled }) => {
  return (
      <Button
          containerStyle={styles.m20}
          titleStyle={styles.colorWhite}
          buttonStyle={styles.backgroundColorblue}
          title={title}
          onPress={onPress}
          disabled={disabled}
      />
  )
}

export default ConfirmButton
