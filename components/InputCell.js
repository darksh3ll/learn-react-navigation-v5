import React from 'react'
import { StyleSheet, TextInput } from 'react-native'

const styles = StyleSheet.create({
  input: {
    height: 50,
    borderColor: '#60499d',
    borderWidth: 2,
    margin: 20,
    padding: 10
  }
})

const InputCell = ({ onChangeText, placeholder, secureTextEntry, textContentType, value, keyboardType, focus, returnKeyType, onSubmitEditing }) => {
  return (
      <TextInput
          autoCapitalize='none'
          textContentType={textContentType}
          secureTextEntry={secureTextEntry}
          placeholderStyle={{ padding: 10 }}
          placeholder={placeholder}
          style={styles.input}
          onChangeText={onChangeText}
          value={value}
          clearButtonMode='always'
          keyboardType={keyboardType}
          ref={focus}
          returnKeyType={returnKeyType}
          onSubmitEditing={onSubmitEditing}
      />
  )
}

export default InputCell
