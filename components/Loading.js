import React  from 'react'
import { ActivityIndicator, View } from 'react-native'

export default ({ loading }) => {
  return (
      loading ? <SpinnerLoading /> : null
  )
}

const SpinnerLoading = () => (
    <View>
      <ActivityIndicator size='large' color='#7b7c7e' />
    </View>

)
