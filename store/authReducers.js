
const initialState = {
  userToken: false
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case 'LOGIN':
      return { ...state, userToken: payload }
    case 'LOGOUT':
      return initialState
    default:
      return state
  }
}

